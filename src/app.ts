import express from 'express';
import cors from 'cors';
import { json } from 'body-parser';
import setupDatabase from '@kominal/service-util/helper/database';
import health from '@kominal/service-util/routes/health';
import swagger from '@kominal/service-util/helper/swagger';

setupDatabase('contacts-service');

const app = express();

app.use(cors());
app.use(
	json({
		limit: '16mb',
	})
);

app.use('/', health);

swagger(app, 'contact-service', 'Contacts Service', 'Manages contacts.');

app.use((req, res) => {
	res.status(404).json({
		message: `Requested route (${req.url}) not found.`,
	});
});

app.listen(3000, () => {
	console.log(`Listening on port 3000`);
});

export default app;
