FROM node:latest
WORKDIR /usr/src/app/
COPY package.json /usr/src/app/
COPY dist/ /usr/src/app/
RUN npm install --production
CMD ["node", "app.js"]